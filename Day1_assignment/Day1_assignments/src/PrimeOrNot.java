//5. Write a java program, which will take a number variable and check whether the number is prime or not.
public class PrimeOrNot {

	public static void main(String[] args) {
		int n =4, i =2;
		boolean flag = false;
	    while (i <= n / 2) {
	      // condition for non prime number
	      if (n% i == 0) {
	        flag = true;
	        break;
	      }

	      ++i;
	    }

	    if (!flag)
	      System.out.println(n + " is a prime number.");
	    else
	      System.out.println(n + " is not a prime number.");
	

	}

}
