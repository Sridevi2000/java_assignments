//9. Write a program to read a number, calculate the sum of squares of even digits (values) present in the given number.
import java.util.Scanner;

public class SumOfSquare {

	public static void main(String[] args) {
	   Scanner sc = new Scanner(System.in);
	   System.out.println("Enter the input number:");
	   int num=sc.nextInt();
	   int n1=0,n2=0;
	   while(num!=0) {
		   n1=num%10;
		   if(n1%2==0)
		   {
			   n2+=n1*n1;
		   }
		   num/=10;
	   }
	   System.out.println("Sum of Squares of Even Digit Value is:"+n2);
	}

}
