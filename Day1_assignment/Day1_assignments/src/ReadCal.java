//8. Write a program to read a number and calculate the sum of odd digits (values) present in the given number.
import java.util.Scanner;
class CheckSum{
	int sumOdd(int x) {
		int sum =0;
		while(x>0) {
			int rem=x%10;
			if(rem%2!=0)
			{
				sum=sum+rem;
			}
			x=x/10;
		}
		return sum;
		
	}
}
public class ReadCal{
	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the input:");
		int n =sc.nextInt();
		CheckSum s= new CheckSum();
		System.out.println("The Sum of odd Digit Values is: "+s.sumOdd(n));
		
		
	}
	
	
}